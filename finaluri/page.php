<link rel="stylesheet" href="style.css">
<?php
   require_once "connection.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Document</title>
   <link rel="stylesheet" href="style.css">
</head>
<body>
   <div class="main">
      <div class="nav">
         <ul>

            <li class="b1"><a href="?menu=select">chamonatvali</a></li>
            <li class="b1"><a href="?menu=insert">damateba</a></li>

         </ul>
      </div>

      <div class="content">
         <?php
            if( (isset($_GET["menu"]) && $_GET["menu"]=="insert") || isset($_POST['insert'])){
               include "insert.php";
            }else if(isset($_GET["menu"]) && $_GET["menu"]=="select"){
               include "administratori.php";
            }else if(isset($_GET["change"]) && $_GET["change"]=="edit"){
                include "edit.php";
            }
         ?>
      </div>
   </div>
</body>
</html>
