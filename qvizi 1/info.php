<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>qvizi 1_1</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    
    <?php
    
    $saxeli = $gvari = $dab_tarigi = $piradi_nomeri = $misamarti = "" ;
    $saxeli_error = $gvari_error = $dab_tarigi_error = $piradi_nomeri_error = $misamarti_error = "" ;


    if(isset($_POST['dadastureba'])) {
    
        if (strlen($saxeli) <= 2 && $saxeli >= 20 ) {
            $saxeli_error = "სახელი უნდა შეიცავდეს არანაკლებ 2-ისა და არაუმეტეს 20 სიმბოლოს რაოდენობას";
        } 
    
        if (strlen($gvari) <= 3 && $gvari <= 50 ) {
            $gvari_error = "გვარი უნდა შეიცავდეს არანაკლებ -ისა და არაუმეტეს 50 სიმბოლოს რაოდენობას";
        }

        if (strlen($piradi_nomeri) != 11 ) {
            $piradi_nomeri = "პირადი ნომერი უნდა შეიცავდეს 11 რიცხვს";
        }

        if (strlen($misamarti) != 11 ) {
            $piradi_nomeri = "მისამართი უნდა შეიცავდეს არაუმეტეს 70 სიმბოლოს";
        }

    }
    ?>

    <form action="action.php" method="POST" >

        <table>

            <tr class="f1">
                <td> 
                    <label for=""> სახელი </label>
                    <br>
                    <input type="text" class="f1" placeholder=" სახელი " name="saxeli" required>
                </td>

                <td> <?=$saxeli_error?> </td>
            </tr>
            

            <tr class="f1">
                <td>
                    <label for=""> გვარი </label>
                    <br>
                    <input type="text" class="f1" placeholder=" გვარი " name="gvari" required>
                </td>

                <td> <?=$gvari_error?> </td>
            </tr>
            

            <tr class="f1">
                <td>
                    <label for=""> დაბადების თარიღი </label>
                    <br>
                    <input type="date" class="f1" placeholder=" დაბადების თარიღი " name="dab_tarigi">
                </td>

                <td>  </td>
            </tr>
            
            
            <tr class="f1">
                <td>
                    <label for=""> პირადი ნომერიი </label>
                    <br>
                    <input type="text" class="f1" placeholder=" პირადი ნომერიი " name="piradi_nomeri" required>
                </td>

                <td> <?=$piradi_nomeri_error?> </td>
            </tr>
            
            
            <tr class="f1">
                <td>
                    <label for=""> მისამართი </label>
                    <br>
                    <input type="text" class="f1" placeholder=" მისამართი " name="misamarti" required>
                </td>

                <td> <?=$misamarti_error?> </td>
            </tr>
            

            <tr class="f1">
                <td>
                    <label for=""> რეგისტრაციის თარიღი </label>
                    <br>
                    <input type="text" class="f1" placeholder=" რეგისტრაციის თარიღი " name="reg_tarigi">
                </td>

                <td>  </td>
            </tr>
            

            <tr class="f1">
                <td>
                    <label for=""> მობილური </label>
                    <br>
                    <input type="text" class="f1" placeholder=" მობილური " name="mobiluri">
                </td>

                <td>  </td>
            </tr>

            
            <tr class="f1">
                <td>
                    <label for=""> დამატებითი ინფორმაცია </label>
                    <br><br>
                    <textarea name="damatebiti_info" rows="5" cols="40"></textarea>
                </td>

                <td>  </td>

            </tr>

        </table>

        <button name="dadastureba"> დადასტურება </button>

    </form>

   


</body>
</html>