<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Write</title>
</head>
<body>
   <?php
      // die("Unable to open file!");
   ?>
   <h1>Write to file</h1>
   <?php
      // $myfile = fopen("files/testfile.txt", "a") or die("Unable to open file!");
      // echo $myfile;
      // fwrite($myfile, "Hello World");
      // fwrite($myfile, "\n");
      // fwrite($myfile, "Hello PHP");
      // fwrite($myfile, "Hello Web");
      // fclose($myfile);
      $patch = "files/test.txt";
      $myfile = fopen($patch, "r") or die("Unable to open file!");
      echo filesize($patch);
      echo "<hr>";
      // echo fread($myfile,filesize($patch));
      echo "<hr>";
      // echo fgets($myfile);
      // echo "<br>";
      // echo fgets($myfile);
      // echo "<br>";
      // echo fgets($myfile);
      // while(!feof($myfile)) {
      //    echo fgets($myfile) . "<br>";
      // }
      echo "<hr>";
      // echo fgetc($myfile);
      // echo fgetc($myfile);
      while(!feof($myfile)) {
         echo fgetc($myfile) . "<br>";
      }
      fclose($myfile);
   ?>
</body>
</html>
