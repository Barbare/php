<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>davaleba 1_2</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    
    <form method="POST">
        სახელი - <input type="text" name="saxeli">
        <br><br>

        გვარი - <input type="text" name="gvari">
        <br><br>

        კურსი - <input type="number" min="1" max="4" name="kursi">
        <br><br>

        სემესტრი - <input type="number" min="1" max="2" name="semestri">
        <br><br>

        სასწავლო კურსი - <input type="text" name="kursis_saxeli">
        <br><br>

        მიღებული ნიშანი - <input type="number" min="1" max="100" name="qula">
        <br><br>

        ლექტორის სახელი - <input type="text" name="leq_sax">
        <br><br>

        ლექტორის გვარი - <input type="text" name="leq_gv">
        <br><br>

        დეკანის სახელი - <input type="text" name="dek_sax">
        <br><br>

        დეკანის გვარი - <input type="text" name="dek_gv">
        <br><br>

        <button name="gamotvla" class="b1"> გამოთვლა </button>
        <button name="ganaxleba" class="b2"> <a href="page.php" > განახლება </a></button>
    </form>
    <br><br>

    <?php
        if (isset($_POST["gamotvla"]))
        {
    
            if ($_POST["qula"] >= 91) {
                $x = "A" ;
            }
            
            else if ($_POST["qula"] >= 81) {
                $x = "B" ;
            }

            else if ($_POST["qula"] >= 71) {
                $x = "C" ;
            }

            else if ($_POST["qula"] >= 61) {
                $x = "D" ;
            }

            else if ($_POST["qula"] >= 51) {
                $x = "E" ;
            }

            else if ($_POST["qula"] >= 41) {
                $x = "FX" ;
            }

            else {
                $x = "F" ;
            }
            
    ?>

    <table>
        <tr>
            <th>სახელი</th>
            <th>გვარი</th>
            <th>კურსი</th>
            <th>სემესტი</th>
            <th>კურსის სახელი</th>
            <th>მიღებული ნიშანი</th>
            <th>შეფასება</th>
            <th>ლექტორის სახელი</th>
            <th>ლექტორის გვარი</th>
            <th>დეკანის სახელი</th>
            <th>დეკანის გვარი</th>
        </tr>

        <tr>
            <td> <?=$_POST["saxeli"]?> </td>
            <td> <?=$_POST["gvari"]?> </td>
            <td> <?=$_POST["kursi"]?> </td>
            <td> <?=$_POST["semestri"]?> </td>
            <td> <?=$_POST["kursis_saxeli"]?> </td>
            <td> <?=$_POST["qula"]?> </td>
            <td> <?=$x?> </td>
            <td> <?=$_POST["leq_sax"]?> </td>
            <td> <?=$_POST["leq_gv"]?> </td>
            <td> <?=$_POST["leq_sax"]?> </td>
            <td> <?=$_POST["leq_gv"]?> </td>
            <td> <?=$_POST["dek_sax"]?> </td>
            <td> <?=$_POST["dek_gv"]?> </td>
        </tr>
    </table>    

    <?php
        }
    ?>

</body>
</html>