-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2022 at 01:49 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mydata`
--

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `ID` int(4) NOT NULL,
  `Title` varchar(30) NOT NULL,
  `Date` date NOT NULL,
  `Type` varchar(30) NOT NULL,
  `Photo` varchar(50) NOT NULL,
  `Text` text NOT NULL,
  `Autor` varchar(30) NOT NULL,
  `Description` varchar(200) NOT NULL,
  `Meta_k` varchar(200) NOT NULL,
  `Meta_d` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `Id` int(4) NOT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `Meta_k` varchar(200) DEFAULT NULL,
  `Meta_d` varchar(200) DEFAULT NULL,
  `Text` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`Id`, `Title`, `Meta_k`, `Meta_d`, `Text`) VALUES
(1, 'html', 'div, a, form,', 'meta_d 1', 'text 1'),
(3, 'title 2', 'meta_k 2 ', 'meta_d 2', 'text is here '),
(5, 'ttitle 3', 'meta_k 3', 'meta_d 3', 'text text'),
(6, 'title 4', 'meta_k 4', 'meta_d 4', 'texto texto'),
(7, 'title 5', 'meta_k 5', 'meta_d 5', 'text was here ');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID` int(4) NOT NULL,
  `Name` varchar(30) DEFAULT NULL,
  `Lastname` varchar(40) DEFAULT NULL,
  `Age` int(11) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `Reg_Date` date DEFAULT NULL,
  `Password` varchar(50) DEFAULT NULL,
  `Gender` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `Name`, `Lastname`, `Age`, `Date`, `Reg_Date`, `Password`, `Gender`) VALUES
(1, 'ana', 'anadze', 19, '2002-06-19', '2022-06-02', 'anaanadze', 'mdedrobiti'),
(2, 'mari', 'maridze', 20, '2005-06-02', '2022-06-01', 'marimaridze', 'mdedrobiti'),
(3, 'gio', 'giodze', 21, '2005-06-08', '2022-06-11', 'giogiodze', 'mamrobiti'),
(4, 'nika', 'nikadze', 19, '2002-06-09', '2022-06-09', 'nikanikadze', 'mamrobiti'),
(5, 'elene', 'elenadze', 20, '2002-06-03', '2022-06-02', 'eleneelenadze', 'mdedrobiti');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data`
--
ALTER TABLE `data`
  MODIFY `ID` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `Id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
