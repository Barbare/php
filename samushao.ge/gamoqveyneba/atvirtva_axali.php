<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>atvirtva</title>
    <link rel="stylesheet" href="atvirtva.css">
    <link rel="icon" href="./potoebi/fuuli.png">
</head>
<body>
    
    <div class="gilakebi">
        <a href="../mtavari/mtavari.php" class="previous round a_hover">&#8249;</a>

        <label class="switch">
            <input type="checkbox" onclick="d_mode()">
            <span class="slider round"></span>
        </label>
    </div>

    <!-- <form class="d0" action="../mtavari/mtavari.php">  -->
        <form action="../vakansia/vakansia_axali.php" method="POST" class="d0">
            <div class="f0">
                <label for="">დამსაქმებელი</label>
                <input type="text" class="f1" placeholder="დამსაქმებელი" name="damsaqmebeli">
            </div>

            <div class="f0">
                <label for="">შემოთავაზება</label>
                <input type="text" class="f1" placeholder="შემოთავაზება" name="shemotavazeba">
            </div>

            <div class="f0">
                <label for="">ანაზღაურება</label>
                <input type="text" class="f1" placeholder="ანაზღაურება" name="anazgaureba">
            </div>

            <div class="f0">
                <label for="">მოკლე ინფო</label>
                <input type="text" class="f1" placeholder="მოკლე ინფო" name="mokle_info">
            </div>

            <div class="f0">
                <label for="">ელ_ფოსტა</label>
                <input type="email" class="f1" placeholder="ელ_ფოსტა" name="el_fostaa">
            </div>
            
            <br>

            <div class="f0">
                <a href="../vakansia/vakansia.php"><input type="submit" class="b1" value="გამოქვეყნება" name="gamoqveyneba"></a>
            </div>
    </form>

            
        </div>
    </form>


    <script src="avtorizacia.js"></script>

</body>
</html>