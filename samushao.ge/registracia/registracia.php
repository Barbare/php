<?php 
session_start();

	include("connection.php");
	include("functions.php");
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>registracia</title>
    <link rel="stylesheet" href="registracia.css">
    <link rel="icon" href="./potoebi/fuuli.png">

</head>
<body>

    <div class="gilakebi">
        <a href="../mtavari/mtavari.php" class="previous round a_hover">&#8249;</a>

        <label class="switch">
            <input type="checkbox" onclick="d_mode()">
            <span class="slider round"></span>
        </label>
    </div>

    <?php
        $el_fosta = $tel_nomeri = $paroli = $gaimeoret_paroli ="" ;
        // $paroli = $_POST['paroli'] = "";
        $el_fosta_err = $paroli_err = $gaimeoret_paroli_err = $kodi_err = $tel_nomeri_err ="" ;
    ?>

    <?php

        $paroli_raodenoba_err = "არანაკლებ 8 სიმბოლო" ;
        $paroli_didiaso_err = "შეიცავდეს დიდ ასოს" ;
        $paroli_patara_aso_err = "შეიცავდეს პატარა ასოს" ;
        $paroli_ricxvi_err = "შეიცავდეს რიცხვს" ;
        $paroli_simbolo_err = "შეიცავდეს დამატებით სიმბოლოს" ;

        if (isset($_POST['dadastureba'])) {
            if (!filter_var($_POST['el_fosta'], FILTER_VALIDATE_EMAIL)) {
                    $el_fosta_err = "მაილი არასწორია"  ;
            }

            if (!preg_match('/^[5][0-9]{8}+$/', $_POST['tel_nomeri'])) {
             
                $tel_nomeri_err = "ტელეფონის ნომერი არასწორადაა შეყვანილი" ;
            }

            if ($_POST['paroli'] != $_POST['gaimeoret_paroli']) {
                $gaimeoret_paroli_err = "პაროლები არ ემთხვევა ერთმანეთს"  ;
            }

            // if (strlen($_POST['paroli']) > 8) {
            //     echo $paroli_raodenoba_err ;       
            // }

            // if (preg_match('@[A-Z]@', $_POST['paroli'])) {
            //     echo $paroli_didiaso_err ;            
            // }

            // if (preg_match('@[a-z]@', $_POST['paroli'])) {
            //     echo $paroli_patara_aso_err ;            
            // }

            // if (preg_match('@[0-9]@', $_POST['paroli'])) {
            //     echo $paroli_ricxvi_err ;            
            // }

            // if (preg_match('@[^\w]@', $_POST['paroli'])) {
            //     echo $paroli_simbolo_err ;             
            // }
        }


// echo "პაროლი: " ;
// echo $paroli_raodenoba ;
// echo $paroli_didiaso ;
// echo $paroli_patara_aso ;
// echo $paroli_ricxvi ;
// echo $paroli_simbolo ;



    ?>

    <form action="" method="POST">
        
        <div class="d0"> 
            <div class="t1">რეგისტრაცია</div>
            
            <div class="d1">
                <div class="f0">
                    <label for="">სახელი</label>
                    <input type="text" class="f1" placeholder="სახელი"  name="saxeli" required>
                </div>

                <div class="f0">
                    <label for="">გვარი</label>
                    <input type="text" class="f1" placeholder="გვარი" name="gvari" required>
                </div>

                <div class="f0">
                    <label for="">ტელ ნომერი</label>
                    <input type="text" class="f1" placeholder="ტელ ნომერი" name="tel_nomeri" required>
                    <div class="err"> <?php  echo $tel_nomeri_err ?> </div>
                </div>

                <div class="f0 chasaweri" id="fosta_id" >
                    <label for="">ელ ფოსტა</label>
                    <input type="email" class="f1" placeholder="ელ ფოსტა" id="fosta_chasaweri" onkeydown="fosta_shemowmeba()" name="el_fosta">
                    <div class="err"> <?php  echo $el_fosta_err  ?> </div>
                    <!-- <div id="fosta_teqsti"></div> -->

                </div>
                
                <div class="f0" required>
                    <label for="">დაბადების თარღი</label>
                    <input type="date" class="f1" placeholder="დაბადების თარღი" name="dab_tarigi">
                </div>

                <div class="f0" required>
                    <label for="">პაროლი</label>
                    <input type="password" class="f1" placeholder="პაროლი" name="paroli" required>
                        <?php  
                            if (isset($_POST['paroli']) && strlen($_POST['paroli']) > 8) {
                                ?> <div class="sworia"> <?php echo $paroli_raodenoba_err ;                     
                                ?> </div> <?php            
                            }
                            else {
                                ?> <div class="err"> <?php echo $paroli_raodenoba_err ?> </div> <?php
                            }

                            if (isset($_POST['paroli']) && preg_match('@[A-Z]@', $_POST['paroli'])) {
                                ?> <div class="sworia"> <?php echo $paroli_didiaso_err ; ?> </div> <?php
                            }
                            else {
                                ?> <div class="err"> <?php echo $paroli_didiaso_err ?> </div> <?php
                            }
                
                            if (isset($_POST['paroli']) && preg_match('@[a-z]@', $_POST['paroli'])) {
                                ?> <div class="sworia"> <?php echo $paroli_patara_aso_err ; ?> </div> <?php
                            }
                            else {
                                ?> <div class="err"> <?php echo $paroli_patara_aso_err ?> </div> <?php
                            }
                
                            if (isset($_POST['paroli']) && preg_match('@[0-9]@', $_POST['paroli'])) {
                                ?> <div class="sworia"> <?php echo $paroli_ricxvi_err ; ?> </div> <?php
                            }
                            else {
                                ?> <div class="err"> <?php echo $paroli_ricxvi_err ?> </div> <?php
                            }
                
                            if (isset($_POST['paroli']) && preg_match('@[^\w]@', $_POST['paroli'])) {
                                ?> <div class="sworia"> <?php echo $paroli_simbolo_err ; ?> </div> <?php
                            }
                            else {
                                ?> <div class="err"> <?php echo $paroli_simbolo_err ?> </div> <?php
                            }
                        ?> 

                </div>

                <div class="f0" required>
                    <label for="">გაიმეორეთ პაროლი</label>
                    <input type="password" class="f1" placeholder="გაიმეორეთ პაროლი" name="gaimeoret_paroli" required>
                    <div class="err"> <?php  echo $gaimeoret_paroli_err ?> </div>
                </div>

                <div class="f0">
                    <a href="../avtorizacia/avtorizacia.php"><input type="submit" class="b1" value="რეგისტრაცია" name="dadastureba"></a>
                </div>

            </div>
        </div>
        <?php
    if(isset($_POST["dadastureba"])){
	// if($_SERVER['REQUEST_METHOD'] == "POST")
	// {
		//something was posted
		$saxeli = $_POST['saxeli'];
		$gvari = $_POST['gvari'];
		$tel_nomeri = $_POST['tel_nomeri'];
		$el_fosta = $_POST['el_fosta'];
		$dab_tarigi = $_POST['dab_tarigi'];
		$paroli = $_POST['paroli'];

		if(!empty($saxeli) && !empty($gvari) && !empty($tel_nomeri) && !empty($el_fosta) && !empty($paroli) && !is_numeric($saxeli) )
		{

			//save to database
			// $query = "INSERT INTO momxmarebeli (saxeli, gvari, tel_nomeri, el_fosta, dab_tarigi, paroli) values ('$id', '$saxeli', '$gvari', '$tel_nomeri', '$el_fosta', '$dab_tarigi', '$paroli',)";

            $sql = ("INSERT INTO momxmarebeli (saxeli, gvari, tel_nomeri, el_fosta, dab_tarigi, paroli) VALUES ('" . $saxeli . "','" . $gvari . "','" . $tel_nomeri . "','" . $el_fosta . "', '" . $dab_tarigi . "','" . $paroli . "')");
            mysqli_query($conn, $sql);

			// mysqli_query($con, $query) ;
            // header("Location: ../avtorizacia/avtorizacia.php") ;
            echo '<meta http-equiv="refresh" content="0; ../avtorizacia/avtorizacia.php">';
            
	}}
?>
    </form>


    <script src="registracia.js"></script>

</body>
</html>