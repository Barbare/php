-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2022 at 04:00 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `samushaoge`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(2) NOT NULL,
  `email` varchar(40) NOT NULL,
  `paroli` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `paroli`) VALUES
(1, 'admin1@gamil.com', 'Paroli123$');

-- --------------------------------------------------------

--
-- Table structure for table `momxmarebeli`
--

CREATE TABLE `momxmarebeli` (
  `id` int(6) NOT NULL,
  `saxeli` varchar(30) NOT NULL,
  `gvari` varchar(40) NOT NULL,
  `tel_nomeri` varchar(10) NOT NULL,
  `el_fosta` varchar(100) NOT NULL,
  `dab_tarigi` date NOT NULL,
  `paroli` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `momxmarebeli`
--

INSERT INTO `momxmarebeli` (`id`, `saxeli`, `gvari`, `tel_nomeri`, `el_fosta`, `dab_tarigi`, `paroli`) VALUES
(1, 'saxeli1', 'gvari1', '599423423', 'saxeli1@gmail.com', '2022-06-10', 'Paroli123$'),
(20310, 's', 'd', '597596411', 'fsaf@gmail.com', '2022-06-03', 'sa'),
(20311, 's', 's', '597596411', 'fsaf@gmail.com', '2022-06-03', 'sd'),
(20312, 'sa', 'gv', '597596411', 'fsaf@gmail.com', '2022-06-01', 'PAroli123$'),
(20313, 'saxeli2', 'gvari2', '599912123', 'fsaf@gmail.com', '2022-06-02', 'Par0li123$');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `momxmarebeli`
--
ALTER TABLE `momxmarebeli`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `momxmarebeli`
--
ALTER TABLE `momxmarebeli`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20314;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
