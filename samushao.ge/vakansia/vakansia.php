<php lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>vakansia</title>
    <link rel="stylesheet" href="vakansia.css" />
    <link rel="icon" href="../mtavari/potoebi/fuuli.png">
  </head>
  <body>

    <!-- menuebi -->
<!-- ==================================================== -->
    <div class="d0">

      <!-- satauri -->
<!-- ---------------------------------------------------- -->
      <div class="dasaxeleba"><a href="../mtavari/mtavari.php"> SAMUSHAO.GE </a></div>
<!-- ---------------------------------------------------- -->


      <!-- kompis menu -->
<!-- ---------------------------------------------------- -->
      <div class="k_menu">
        <ul>
          <li><a href="../mtavari/mtavari.php">მთავარი</a></li>
          <li><a href="../chvens shesaxeb/chvens_shesaxeb.php">ჩვენს შესახებ</a></li>
          <li><a href="../gamoqveyneba/gamoqveyneba.php"> გამოქვეყნება </a></li>
          <li class="log_in"><a href="../avtorizacia/avtorizacia.php" >ავტორიზაცია</a></li>
          <li class="sign_up"><a href="../registracia/registracia.php">რეგისტრაცია</a></li>
          <label class="switch">
            <li class="no_border">
              <input type="checkbox" onclick="d_mode()">
              <div class="slider round"></div>
            </li>
          </label>
        </ul>
      </div>
<!-- ---------------------------------------------------- -->


      <!-- mobiluris menu -->
<!-- ---------------------------------------------------- -->
      <div class="h_icon" onclick="gamoweva(this)">
        <div class="h_1"></div>
        <div class="h_2"></div>
        <div class="h_3"></div>
        <ul class="m_menu">
          <li><a href="../mtavari/mtavari.php">მთავარი</a></li>
          <li><a href="../chvens shesaxeb/chvens_shesaxeb.php">ჩვენს შესახებ</a></li>
          <li><a href="../gamoqveyneba/gamoqveyneba.php"> გამოქვეყნება </a></li>
          <li><a href="../avtorizacia/avtorizacia.php" >ავტორიზაცია</a></li>
          <li><a href="../registracia/registracia.php">რეგისტრაცია</a></li>
          <br>
          <label class="switch">
            <li>
              <input type="checkbox" onclick="d_mode()">
              <div class="slider round"></div>
            </li>
          </label>
        </ul>
      </div>
<!-- ---------------------------------------------------- -->
    </div>
<!-- ==================================================== -->



      <!-- chvens shesaxeb -->
<!-- ==================================================== -->
    <div class="chvens_shesaxeb">
      <p class="p0"> ვაკანსია </p>

      <div class="info">
        <li> ჩვენ გვესაჭიროება ამ პროფესიაში გამოცდილი პიროვნება </li>
        <li> უნდა შეეძლოს ხალხთან კომუნიკაცია </li>
        <li> მნიშვნელოვანი მოთხოვნაა იყოს კრეატიული </li>
        <li> თუ აკმაყოილებთ ჩვენს კრიტერიუმებს დაგვიკავშირდით </li>
        <li> მისამართზე   samushao@gmail.ge </li>
      </div>
    </div>
    
      <div class="dagvikavshirdit">
        <p class="p2"> გთხოვთ, დაგვიკავშირდეთ ელექტრონული ფოსტით მისამართზე: info@samushao.ge. </p>
        <p class="p2"> ფინანსურ საკითხებთან დაკავშირებით, გთხოვთ, მიმართოთ ჩვენს ფინანსურ მენეჯერს მისამართზე: saxeli@samushao.ge </p>
      </div>

 
    <script src="vakansia.js"></script>

  </body>
</html>