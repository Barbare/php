<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>davaleba 2_4</title>
    <link rel="stylesheet" href="style.css">

</head>
<body>

    <?php
        $cars = array(
            array("Make" => "Toyota",
                "Model" => "Corolla",
                "Color" => "White",
                "Mileage" => 24000,
                "Status" => "Sold"),
                
            array("Make" => "Toyota",
                "Model" => "Camry",
                "Color" => "Black",
                "Mileage" => 56000,
                "Status" => "Available"),

            array("Make" => "Honda",
                "Model" => "Accord",
                "Color" => "White",
                "Mileage" => 15000,
                "Status" => "Sold") );


    
    ?>

    <table>
        <tr>
            <th> Make </th>
            <th> Model </th>
            <th> Color </th>
            <th> Mileage </th>
            <th> Status </th>
        </tr>

        <?php
        foreach ($cars as $row) {
            echo '<tr>';
            foreach ($row as $item) {
            echo "<td>{$item}</td>";
            }
            echo '</tr>';
        }
        ?>

    </table>

    

</body>
</html>