<?php 

session_start();

	include("connection.php");
	include("functions.php");


	if($_SERVER['REQUEST_METHOD'] == "POST")
	{
		//something was posted
		$el_fosta = $_POST['el_fosta'];
		$paroli = $_POST['paroli'];

		if(!empty($el_fosta) && !empty($paroli) && !is_numeric($el_fosta))
		{

			//read from database
			$query = "select * from momxmarebeli where el_fosta = '$el_fosta' limit 1";
			$result = mysqli_query($con, $query);

			if($result)
			{
				if($result && mysqli_num_rows($result) > 0)
				{

					$user_data = mysqli_fetch_assoc($result);
					
					if($user_data['paroli'] === $paroli)
					{

						$_SESSION['id'] = $user_data['id'];
						header("Location: ../mtavari/index.php");
						die;
					}
				}
			}
			
			echo "არასწორია მეილი ან პაროლი";
		}else
		{
			echo "არასწორია მეილი ან პაროლი";
		}
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>avtorizacia</title>
    <link rel="stylesheet" href="avtorizacia.css">
    <link rel="icon" href="./potoebi/fuuli.png">
</head>
<body>
    
    <div class="gilakebi">
        <a href="../mtavari/mtavari.php" class="previous round a_hover">&#8249;</a>

        <label class="switch">
            <input type="checkbox" onclick="d_mode()">
            <span class="slider round"></span>
        </label>
    </div>

    <form class="d0" action="../mtavari/mtavari.php"> 
        <div class="t1">ავტორიზაცია</div>
            
            <div class="f0">
                <label for="">ელ ფოსტა</label>
                <input type="email" class="f1" placeholder="ელ ფოსტა">
            </div>

            <div class="f0">
                <label for="">პაროლი</label>
                <input type="password" class="f1" placeholder="პაროლი">
            </div>

            <a href="#" class="dagaviwydat"> დაგავიწყდათ პაროლი ? </a>
            
            <br>

            <a href="../registracia/registracia.php" class="dagaviwydat"> დარეგისტრირება </a>

            <div class="f0">
                <a><input type="submit" class="b1" value="შესვლა"></a>
            </div>

            
        </div>
    </form>


    <script src="avtorizacia.js"></script>

</body>
</html>