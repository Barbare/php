<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>gamoqveyneba</title>
    <link rel="stylesheet" href="gamoqveyneba.css" />
    <link rel="icon" href="./potoebi/fuuli.png">
  </head>
  <body>

    <!-- menuebi -->
<!-- ==================================================== -->
    <div class="d0">

      <!-- satauri -->
<!-- ---------------------------------------------------- -->
      <div class="dasaxeleba"><a href="../mtavari/mtavari.php"> SAMUSHAO.GE </a></div>
<!-- ---------------------------------------------------- -->


      <!-- kompis menu -->
<!-- ---------------------------------------------------- -->
      <div class="k_menu">
        <ul>
          <li><a href="../mtavari/mtavari.php">მთავარი</a></li>
          <li><a href="../chvens shesaxeb/chvens_shesaxeb.php">ჩვენს შესახებ</a></li>
          <li><a href="./gamoqveyneba.php"> გამოქვეყნება </a></li>
          <li class="log_in"><a href="../avtorizacia/avtorizacia.php" >ავტორიზაცია</a></li>
          <li class="sign_up"><a href="../registracia/registracia.php">რეგისტრაცია</a></li>
          <label class="switch">
            <li class="no_border">
              <input type="checkbox" onclick="d_mode()">
              <div class="slider round"></div>
            </li>
          </label>
        </ul>
      </div>
<!-- ---------------------------------------------------- -->


      <!-- mobiluris menu -->
<!-- ---------------------------------------------------- -->
      <div class="h_icon" onclick="gamoweva(this)">
        <div class="h_1"></div>
        <div class="h_2"></div>
        <div class="h_3"></div>
        <ul class="m_menu">
          <li><a href="../mtavari/mtavari.php">მთავარი</a></li>
          <li><a href="../chvens shesaxeb/chvens_shesaxeb.php">ჩვენს შესახებ</a></li>
          <li><a href="./gamoqveyneba.php"> გამოქვეყნება </a></li>
          <li><a href="../avtorizacia/avtorizacia.php" >ავტორიზაცია</a></li>
          <li><a href="../registracia/registracia.php">რეგისტრაცია</a></li>
          <br>
          <label class="switch">
            <li>
              <input type="checkbox" onclick="d_mode()">
              <div class="slider round"></div>
            </li>
          </label>
        </ul>
      </div>
<!-- ---------------------------------------------------- -->
    </div>
<!-- ==================================================== -->




      <!-- tarifebi -->
<!-- ==================================================== -->
    <div class="gamoqveyneba_text">
      <p class="p0"> ტარიფები </p>

      <div class="tarifebi">
        <p class="p1"> super vip განცხადება </p>
        <li> განცხადება იქნება გამოქვეყნებული სლაიდერის სახით, რის გამოც თქვენი განცხადების არ შემჩნევა ნამდვილად რთული იქნება </li>
        <li> ფასი  50 ლარი </li>
        <li> ხანგრძლივობა 1 კვირა </li>
        <button class="b1" name="gamoqveyneba"> არჩევა </button>
      </div>

      <div class="tarifebi">
        <p class="p1"> vip განცხადება </p>
        <li> განცხადება იქნება გამოქვეყნებული როგორც top 10 განცხადება, რაც თქვენი განცხადების ნახვის შანს ზრდის </li>
        <li> ფასი 40 ლარი </li>
        <li> ხანგრძლივობა 1 კვირა </li>
        <button class="b1" name="gamoqveyneba"> არჩევა </button>
      </div>
      
      <div class="tarifebi">
        <p class="p1"> უბრალო განცხადება </p>
        <li> განცხადება იქნება გამოქვეყნებული როგორც ჩვეულებრივი განცხადება </li>
        <li> ფასი 30 ლარი </li>
        <li> ხანგრძლივობა 1 კვირა </li>
        <button class="b1" name="gamoqveyneba"> არჩევა </button>
      </div>
    </div>
<!-- ==================================================== -->




      <!-- gamoqveyneba -->
<!-- ==================================================== -->
    <div  class="gamoqveyneba">
      <p class="p2"> გამოაქვეყნე განცხადება </p>
      <li> გამოგზავნეთ თქვენი განცხადების ტექსტი მისამართზე: publish@samushao.ge ან info@samushao.ge  </li>
    </div>
  

<!-- ==================================================== -->

 
    <script src="gamoqveyneba.js"></script>

  </body>
</html>