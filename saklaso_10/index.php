<?php
   session_start();
   require_once "connection.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Document</title>
   <link rel="stylesheet" href="style.css">
</head>
<body>
   <div class="main">
      <div class="nav">
         <ul>
            <li><a href="index.php">HOME</a></li>
            <?php
                if(isset($_SESSION['email'])){
            ?>
            <li><a href="?menu=select">ADMIN</a></li>
            <li><a href="?menu=insert">INSERT</a></li>
             <?php
                }
             ?>
            <!--<li><a href="?menu=update">UPDATE</a></li>
            <li><a href="?menu=delete">DELETE</a></li>-->
             <li style="height: 40px"></li>
             <?php
                if(!isset($_SESSION['email'])){
             ?>
             <li><a href="?menu=signin">SIGN IN</a></li>
             <li><a href="?menu=signup">SIGN UP</a></li>

             <?php
                }else{
             ?>
             <li style="margin-left: -20px"><?=$_SESSION['email']?></li>
             <li><a href="?menu=signout">SIGN OUT</a></li>
             <?php
                }
             ?>
         </ul>
      </div>
      <div class="content">
         <?php
            if( (isset($_GET["menu"]) && $_GET["menu"]=="insert") || isset($_POST['insert']))
            {
               include "menu/insert.php";
            }
            else if(isset($_GET["menu"]) && $_GET["menu"]=="select")
            {
               include "menu/select.php";
            }
            else if(isset($_GET["menu"]) && $_GET["menu"]=="signup")
            {
                include "menu/signup.php";
            }
            else if(isset($_GET["menu"]) && $_GET["menu"]=="signin")
            {
                include "menu/signin.php";
            }
            else if(isset($_GET["menu"]) && $_GET["menu"]=="signout")
            {
                unset($_SESSION['email']);
                header("location: index.php");
            }
            else if(isset($_GET["change"]) && $_GET["change"]=="edit")
            {
                include "menu/edit.php";
            }
            else
            {
                include "menu/home.php";
            }
         ?>
      </div>
   </div>
</body>
</html>
