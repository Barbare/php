<?php

    session_start();

?>

<h1>Page 1</h1>
<br><br>
<a href="page2.php">Page2</a>
<br><br>
<a href="page3.php">Page3</a>
<br><br>
<a href="page4.php">Page4</a>
<hr>

<?php

    $x1 = 78;

    echo "Local -> ".$x1;
    echo "<br>";

    $_SESSION['x2'] = 79;
    $_SESSION['x3'] = 80;
    $_SESSION['x4'] = 81;

    echo "Session -> ".$_SESSION['x2'];
    echo "<br>";
    echo "Session -> ".$_SESSION['x3'];
    echo "<br>";
    echo "Session -> ".$_SESSION['x4'];
    echo "<br>";

?>
